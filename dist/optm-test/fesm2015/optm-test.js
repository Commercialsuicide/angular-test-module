import { Injectable, Component, Input, NgModule, defineInjectable } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OptmTestService {
    constructor() { }
}
OptmTestService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
OptmTestService.ctorParameters = () => [];
/** @nocollapse */ OptmTestService.ngInjectableDef = defineInjectable({ factory: function OptmTestService_Factory() { return new OptmTestService(); }, token: OptmTestService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OptmTestComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
OptmTestComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-optm-test',
                template: `
    <div class="lib-optm-test">
      <p *ngIf="initial">{{initial * 2}}</p>
    </div>
  `,
                styles: [`
    .lib-optm-test {
      min-width: 150px;
      height: 150px;
      background-color: royalblue;
      color: orange;
      font-size: 50px;
      padding: 50px;
    }
  `]
            }] }
];
/** @nocollapse */
OptmTestComponent.ctorParameters = () => [];
OptmTestComponent.propDecorators = {
    initial: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OptmTestModule {
}
OptmTestModule.decorators = [
    { type: NgModule, args: [{
                declarations: [OptmTestComponent],
                imports: [CommonModule],
                exports: [OptmTestComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { OptmTestService, OptmTestComponent, OptmTestModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0bS10ZXN0LmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9vcHRtLXRlc3QvbGliL29wdG0tdGVzdC5zZXJ2aWNlLnRzIiwibmc6Ly9vcHRtLXRlc3QvbGliL29wdG0tdGVzdC5jb21wb25lbnQudHMiLCJuZzovL29wdG0tdGVzdC9saWIvb3B0bS10ZXN0Lm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE9wdG1UZXN0U2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItb3B0bS10ZXN0JyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8ZGl2IGNsYXNzPVwibGliLW9wdG0tdGVzdFwiPlxuICAgICAgPHAgKm5nSWY9XCJpbml0aWFsXCI+e3tpbml0aWFsICogMn19PC9wPlxuICAgIDwvZGl2PlxuICBgLFxuICBzdHlsZXM6IFtgXG4gICAgLmxpYi1vcHRtLXRlc3Qge1xuICAgICAgbWluLXdpZHRoOiAxNTBweDtcbiAgICAgIGhlaWdodDogMTUwcHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByb3lhbGJsdWU7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgICBmb250LXNpemU6IDUwcHg7XG4gICAgICBwYWRkaW5nOiA1MHB4O1xuICAgIH1cbiAgYF1cbn0pXG5leHBvcnQgY2xhc3MgT3B0bVRlc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBpbml0aWFsOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE9wdG1UZXN0Q29tcG9uZW50IH0gZnJvbSAnLi9vcHRtLXRlc3QuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbT3B0bVRlc3RDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgZXhwb3J0czogW09wdG1UZXN0Q29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBPcHRtVGVzdE1vZHVsZSB7IH1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsTUFLYSxlQUFlO0lBRTFCLGlCQUFpQjs7O1lBTGxCLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7Ozs7OztBQ0pELE1Bb0JhLGlCQUFpQjtJQUc1QixpQkFBaUI7Ozs7SUFFakIsUUFBUTtLQUNQOzs7WUF4QkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxlQUFlO2dCQUN6QixRQUFRLEVBQUU7Ozs7R0FJVDt5QkFDUTs7Ozs7Ozs7O0dBU1I7YUFDRjs7Ozs7c0JBRUUsS0FBSzs7Ozs7OztBQ3JCUixNQVNhLGNBQWM7OztZQUwxQixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7Z0JBQ2pDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDdkIsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7YUFDN0I7Ozs7Ozs7Ozs7Ozs7OzsifQ==