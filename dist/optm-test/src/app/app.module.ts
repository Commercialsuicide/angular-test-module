import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { OptmTestModule } from 'optm-test';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    OptmTestModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
