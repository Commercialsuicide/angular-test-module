(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('optm-test', ['exports', '@angular/core', '@angular/common'], factory) :
    (factory((global['optm-test'] = {}),global.ng.core,global.ng.common));
}(this, (function (exports,i0,common) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var OptmTestService = /** @class */ (function () {
        function OptmTestService() {
        }
        OptmTestService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        OptmTestService.ctorParameters = function () { return []; };
        /** @nocollapse */ OptmTestService.ngInjectableDef = i0.defineInjectable({ factory: function OptmTestService_Factory() { return new OptmTestService(); }, token: OptmTestService, providedIn: "root" });
        return OptmTestService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var OptmTestComponent = /** @class */ (function () {
        function OptmTestComponent() {
        }
        /**
         * @return {?}
         */
        OptmTestComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        OptmTestComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'lib-optm-test',
                        template: "\n    <div class=\"lib-optm-test\">\n      <p *ngIf=\"initial\">{{initial * 2}}</p>\n    </div>\n  ",
                        styles: ["\n    .lib-optm-test {\n      min-width: 150px;\n      height: 150px;\n      background-color: royalblue;\n      color: orange;\n      font-size: 50px;\n      padding: 50px;\n    }\n  "]
                    }] }
        ];
        /** @nocollapse */
        OptmTestComponent.ctorParameters = function () { return []; };
        OptmTestComponent.propDecorators = {
            initial: [{ type: i0.Input }]
        };
        return OptmTestComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var OptmTestModule = /** @class */ (function () {
        function OptmTestModule() {
        }
        OptmTestModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [OptmTestComponent],
                        imports: [common.CommonModule],
                        exports: [OptmTestComponent]
                    },] }
        ];
        return OptmTestModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.OptmTestService = OptmTestService;
    exports.OptmTestComponent = OptmTestComponent;
    exports.OptmTestModule = OptmTestModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0bS10ZXN0LnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vb3B0bS10ZXN0L2xpYi9vcHRtLXRlc3Quc2VydmljZS50cyIsIm5nOi8vb3B0bS10ZXN0L2xpYi9vcHRtLXRlc3QuY29tcG9uZW50LnRzIiwibmc6Ly9vcHRtLXRlc3QvbGliL29wdG0tdGVzdC5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBPcHRtVGVzdFNlcnZpY2Uge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG59XG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLW9wdG0tdGVzdCcsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGRpdiBjbGFzcz1cImxpYi1vcHRtLXRlc3RcIj5cbiAgICAgIDxwICpuZ0lmPVwiaW5pdGlhbFwiPnt7aW5pdGlhbCAqIDJ9fTwvcD5cbiAgICA8L2Rpdj5cbiAgYCxcbiAgc3R5bGVzOiBbYFxuICAgIC5saWItb3B0bS10ZXN0IHtcbiAgICAgIG1pbi13aWR0aDogMTUwcHg7XG4gICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcm95YWxibHVlO1xuICAgICAgY29sb3I6IG9yYW5nZTtcbiAgICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICAgIHBhZGRpbmc6IDUwcHg7XG4gICAgfVxuICBgXVxufSlcbmV4cG9ydCBjbGFzcyBPcHRtVGVzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGluaXRpYWw6IG51bWJlcjtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgT3B0bVRlc3RDb21wb25lbnQgfSBmcm9tICcuL29wdG0tdGVzdC5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtPcHRtVGVzdENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxuICBleHBvcnRzOiBbT3B0bVRlc3RDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIE9wdG1UZXN0TW9kdWxlIHsgfVxuIl0sIm5hbWVzIjpbIkluamVjdGFibGUiLCJDb21wb25lbnQiLCJJbnB1dCIsIk5nTW9kdWxlIiwiQ29tbW9uTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7UUFPRTtTQUFpQjs7b0JBTGxCQSxhQUFVLFNBQUM7d0JBQ1YsVUFBVSxFQUFFLE1BQU07cUJBQ25COzs7Ozs4QkFKRDtLQUVBOzs7Ozs7QUNGQTtRQXVCRTtTQUFpQjs7OztRQUVqQixvQ0FBUTs7O1lBQVI7YUFDQzs7b0JBeEJGQyxZQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGVBQWU7d0JBQ3pCLFFBQVEsRUFBRSxxR0FJVDtpQ0FDUSwwTEFTUjtxQkFDRjs7Ozs7OEJBRUVDLFFBQUs7O1FBT1Isd0JBQUM7S0ExQkQ7Ozs7OztBQ0ZBO1FBSUE7U0FLK0I7O29CQUw5QkMsV0FBUSxTQUFDO3dCQUNSLFlBQVksRUFBRSxDQUFDLGlCQUFpQixDQUFDO3dCQUNqQyxPQUFPLEVBQUUsQ0FBQ0MsbUJBQVksQ0FBQzt3QkFDdkIsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7cUJBQzdCOztRQUM2QixxQkFBQztLQUwvQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9