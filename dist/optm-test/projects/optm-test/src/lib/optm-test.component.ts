import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-optm-test',
  template: `
    <div class="lib-optm-test">
      <p *ngIf="initial">{{initial * 2}}</p>
    </div>
  `,
  styles: [`
    .lib-optm-test {
      min-width: 150px;
      height: 150px;
      background-color: royalblue;
      color: white;
      font-size: 50px;
      padding: 50px;
    }
  `]
})
export class OptmTestComponent implements OnInit {
  @Input() initial: number;

  constructor() { }

  ngOnInit() {
  }

}
