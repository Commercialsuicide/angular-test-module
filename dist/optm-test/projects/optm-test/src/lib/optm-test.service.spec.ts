import { TestBed } from '@angular/core/testing';

import { OptmTestService } from './optm-test.service';

describe('OptmTestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OptmTestService = TestBed.get(OptmTestService);
    expect(service).toBeTruthy();
  });
});
