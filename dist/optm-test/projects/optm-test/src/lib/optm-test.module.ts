import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptmTestComponent } from './optm-test.component';

@NgModule({
  declarations: [OptmTestComponent],
  imports: [CommonModule],
  exports: [OptmTestComponent]
})
export class OptmTestModule { }
