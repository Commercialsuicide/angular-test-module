import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptmTestComponent } from './optm-test.component';

describe('OptmTestComponent', () => {
  let component: OptmTestComponent;
  let fixture: ComponentFixture<OptmTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptmTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptmTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
