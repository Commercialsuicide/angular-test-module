/*
 * Public API Surface of optm-test
 */

export * from './lib/optm-test.service';
export * from './lib/optm-test.component';
export * from './lib/optm-test.module';
