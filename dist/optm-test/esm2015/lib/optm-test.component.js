/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class OptmTestComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
OptmTestComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-optm-test',
                template: `
    <div class="lib-optm-test">
      <p *ngIf="initial">{{initial * 2}}</p>
    </div>
  `,
                styles: [`
    .lib-optm-test {
      min-width: 150px;
      height: 150px;
      background-color: royalblue;
      color: orange;
      font-size: 50px;
      padding: 50px;
    }
  `]
            }] }
];
/** @nocollapse */
OptmTestComponent.ctorParameters = () => [];
OptmTestComponent.propDecorators = {
    initial: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    OptmTestComponent.prototype.initial;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0bS10ZXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL29wdG0tdGVzdC8iLCJzb3VyY2VzIjpbImxpYi9vcHRtLXRlc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQW9CekQsTUFBTSxPQUFPLGlCQUFpQjtJQUc1QixnQkFBZ0IsQ0FBQzs7OztJQUVqQixRQUFRO0lBQ1IsQ0FBQzs7O1lBeEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsUUFBUSxFQUFFOzs7O0dBSVQ7eUJBQ1E7Ozs7Ozs7OztHQVNSO2FBQ0Y7Ozs7O3NCQUVFLEtBQUs7Ozs7SUFBTixvQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLW9wdG0tdGVzdCcsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGRpdiBjbGFzcz1cImxpYi1vcHRtLXRlc3RcIj5cbiAgICAgIDxwICpuZ0lmPVwiaW5pdGlhbFwiPnt7aW5pdGlhbCAqIDJ9fTwvcD5cbiAgICA8L2Rpdj5cbiAgYCxcbiAgc3R5bGVzOiBbYFxuICAgIC5saWItb3B0bS10ZXN0IHtcbiAgICAgIG1pbi13aWR0aDogMTUwcHg7XG4gICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogcm95YWxibHVlO1xuICAgICAgY29sb3I6IG9yYW5nZTtcbiAgICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICAgIHBhZGRpbmc6IDUwcHg7XG4gICAgfVxuICBgXVxufSlcbmV4cG9ydCBjbGFzcyBPcHRtVGVzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGluaXRpYWw6IG51bWJlcjtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbn1cbiJdfQ==