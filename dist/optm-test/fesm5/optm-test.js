import { Injectable, Component, Input, NgModule, defineInjectable } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var OptmTestService = /** @class */ (function () {
    function OptmTestService() {
    }
    OptmTestService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    OptmTestService.ctorParameters = function () { return []; };
    /** @nocollapse */ OptmTestService.ngInjectableDef = defineInjectable({ factory: function OptmTestService_Factory() { return new OptmTestService(); }, token: OptmTestService, providedIn: "root" });
    return OptmTestService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var OptmTestComponent = /** @class */ (function () {
    function OptmTestComponent() {
    }
    /**
     * @return {?}
     */
    OptmTestComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    OptmTestComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-optm-test',
                    template: "\n    <div class=\"lib-optm-test\">\n      <p *ngIf=\"initial\">{{initial * 2}}</p>\n    </div>\n  ",
                    styles: ["\n    .lib-optm-test {\n      min-width: 150px;\n      height: 150px;\n      background-color: royalblue;\n      color: orange;\n      font-size: 50px;\n      padding: 50px;\n    }\n  "]
                }] }
    ];
    /** @nocollapse */
    OptmTestComponent.ctorParameters = function () { return []; };
    OptmTestComponent.propDecorators = {
        initial: [{ type: Input }]
    };
    return OptmTestComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var OptmTestModule = /** @class */ (function () {
    function OptmTestModule() {
    }
    OptmTestModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [OptmTestComponent],
                    imports: [CommonModule],
                    exports: [OptmTestComponent]
                },] }
    ];
    return OptmTestModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { OptmTestService, OptmTestComponent, OptmTestModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0bS10ZXN0LmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9vcHRtLXRlc3QvbGliL29wdG0tdGVzdC5zZXJ2aWNlLnRzIiwibmc6Ly9vcHRtLXRlc3QvbGliL29wdG0tdGVzdC5jb21wb25lbnQudHMiLCJuZzovL29wdG0tdGVzdC9saWIvb3B0bS10ZXN0Lm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE9wdG1UZXN0U2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItb3B0bS10ZXN0JyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8ZGl2IGNsYXNzPVwibGliLW9wdG0tdGVzdFwiPlxuICAgICAgPHAgKm5nSWY9XCJpbml0aWFsXCI+e3tpbml0aWFsICogMn19PC9wPlxuICAgIDwvZGl2PlxuICBgLFxuICBzdHlsZXM6IFtgXG4gICAgLmxpYi1vcHRtLXRlc3Qge1xuICAgICAgbWluLXdpZHRoOiAxNTBweDtcbiAgICAgIGhlaWdodDogMTUwcHg7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByb3lhbGJsdWU7XG4gICAgICBjb2xvcjogb3JhbmdlO1xuICAgICAgZm9udC1zaXplOiA1MHB4O1xuICAgICAgcGFkZGluZzogNTBweDtcbiAgICB9XG4gIGBdXG59KVxuZXhwb3J0IGNsYXNzIE9wdG1UZXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgaW5pdGlhbDogbnVtYmVyO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBPcHRtVGVzdENvbXBvbmVudCB9IGZyb20gJy4vb3B0bS10ZXN0LmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW09wdG1UZXN0Q29tcG9uZW50XSxcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV0sXG4gIGV4cG9ydHM6IFtPcHRtVGVzdENvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgT3B0bVRlc3RNb2R1bGUgeyB9XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0lBT0U7S0FBaUI7O2dCQUxsQixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7OzswQkFKRDtDQUVBOzs7Ozs7QUNGQTtJQXVCRTtLQUFpQjs7OztJQUVqQixvQ0FBUTs7O0lBQVI7S0FDQzs7Z0JBeEJGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsUUFBUSxFQUFFLHFHQUlUOzZCQUNRLDBMQVNSO2lCQUNGOzs7OzswQkFFRSxLQUFLOztJQU9SLHdCQUFDO0NBMUJEOzs7Ozs7QUNGQTtJQUlBO0tBSytCOztnQkFMOUIsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLGlCQUFpQixDQUFDO29CQUNqQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3ZCLE9BQU8sRUFBRSxDQUFDLGlCQUFpQixDQUFDO2lCQUM3Qjs7SUFDNkIscUJBQUM7Q0FML0I7Ozs7Ozs7Ozs7Ozs7OyJ9