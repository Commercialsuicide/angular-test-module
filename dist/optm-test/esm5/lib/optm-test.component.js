/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var OptmTestComponent = /** @class */ (function () {
    function OptmTestComponent() {
    }
    /**
     * @return {?}
     */
    OptmTestComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    OptmTestComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-optm-test',
                    template: "\n    <div class=\"lib-optm-test\">\n      <p *ngIf=\"initial\">{{initial * 2}}</p>\n    </div>\n  ",
                    styles: ["\n    .lib-optm-test {\n      min-width: 150px;\n      height: 150px;\n      background-color: royalblue;\n      color: orange;\n      font-size: 50px;\n      padding: 50px;\n    }\n  "]
                }] }
    ];
    /** @nocollapse */
    OptmTestComponent.ctorParameters = function () { return []; };
    OptmTestComponent.propDecorators = {
        initial: [{ type: Input }]
    };
    return OptmTestComponent;
}());
export { OptmTestComponent };
if (false) {
    /** @type {?} */
    OptmTestComponent.prototype.initial;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0bS10ZXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL29wdG0tdGVzdC8iLCJzb3VyY2VzIjpbImxpYi9vcHRtLXRlc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6RDtJQXFCRTtJQUFnQixDQUFDOzs7O0lBRWpCLG9DQUFROzs7SUFBUjtJQUNBLENBQUM7O2dCQXhCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLFFBQVEsRUFBRSxxR0FJVDs2QkFDUSwwTEFTUjtpQkFDRjs7Ozs7MEJBRUUsS0FBSzs7SUFPUix3QkFBQztDQUFBLEFBMUJELElBMEJDO1NBUlksaUJBQWlCOzs7SUFDNUIsb0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1vcHRtLXRlc3QnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxkaXYgY2xhc3M9XCJsaWItb3B0bS10ZXN0XCI+XG4gICAgICA8cCAqbmdJZj1cImluaXRpYWxcIj57e2luaXRpYWwgKiAyfX08L3A+XG4gICAgPC9kaXY+XG4gIGAsXG4gIHN0eWxlczogW2BcbiAgICAubGliLW9wdG0tdGVzdCB7XG4gICAgICBtaW4td2lkdGg6IDE1MHB4O1xuICAgICAgaGVpZ2h0OiAxNTBweDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHJveWFsYmx1ZTtcbiAgICAgIGNvbG9yOiBvcmFuZ2U7XG4gICAgICBmb250LXNpemU6IDUwcHg7XG4gICAgICBwYWRkaW5nOiA1MHB4O1xuICAgIH1cbiAgYF1cbn0pXG5leHBvcnQgY2xhc3MgT3B0bVRlc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBpbml0aWFsOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG59XG4iXX0=