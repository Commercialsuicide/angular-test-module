/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptmTestComponent } from './optm-test.component';
var OptmTestModule = /** @class */ (function () {
    function OptmTestModule() {
    }
    OptmTestModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [OptmTestComponent],
                    imports: [CommonModule],
                    exports: [OptmTestComponent]
                },] }
    ];
    return OptmTestModule;
}());
export { OptmTestModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3B0bS10ZXN0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL29wdG0tdGVzdC8iLCJzb3VyY2VzIjpbImxpYi9vcHRtLXRlc3QubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUUxRDtJQUFBO0lBSzhCLENBQUM7O2dCQUw5QixRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsaUJBQWlCLENBQUM7b0JBQ2pDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDdkIsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7aUJBQzdCOztJQUM2QixxQkFBQztDQUFBLEFBTC9CLElBSytCO1NBQWxCLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE9wdG1UZXN0Q29tcG9uZW50IH0gZnJvbSAnLi9vcHRtLXRlc3QuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbT3B0bVRlc3RDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgZXhwb3J0czogW09wdG1UZXN0Q29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBPcHRtVGVzdE1vZHVsZSB7IH1cbiJdfQ==